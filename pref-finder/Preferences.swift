//
//  Preferences.swift
//  pref-finder
//
//  Created by Joel Rennich on 3/24/17.
//  Copyright © 2017 Trusource Labs. All rights reserved.
//

import Foundation

class Preferences {

    var defaults: UserDefaults
    var allItems: [String: Any]
    var localSettings: [String: Any]

    init?(domain: String) {
        guard let defaults = UserDefaults.init(suiteName: domain),
        let local = UserDefaults.standard.persistentDomain(forName: domain) else { return nil }

        self.defaults = defaults
        self.localSettings = local
        self.allItems = defaults.dictionaryRepresentation()
    }

    func printAll() {
        print("Local Keys:")
        print("\(localSettings)\n")
        checkForcedAll()
    }

    func printKey(key: String) {
        print(defaults.string(forKey: key) ?? "No settings found.")
    }

    func checkForced(key: String) -> Bool {
        return defaults.objectIsForced(forKey: key)
    }

    func checkForcedAll() {
        print("Forced Keys:")
        for item in allItems {
            if defaults.objectIsForced(forKey: item.key) {
                print(" " + (item.key) + " is forced to: " + String(describing: item.value))
            }
        }
    }
}
